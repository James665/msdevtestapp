package com.example.jamesb.msdevtestapp.database;

import android.provider.BaseColumns;


public class Contract {

    public Contract() {
    }

    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "feed_entry";
        public static final String LOCATION = "location";
        public static final String KEY_IMAGE = "key_image";
        public static final String ID = BaseColumns._ID;

        public static final String SQL_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        ID + "  INTEGER PRIMARY KEY, " +
                        LOCATION + " TEXT, " +
                        KEY_IMAGE + " BLOB);";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


}
