package com.example.jamesb.msdevtestapp.utils;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.jamesb.msdevtestapp.database.Contract;

import java.util.ArrayList;

public class DatabaseUtils {


    public DatabaseUtils() {

    }

    public void addDataToDatabase(String name, byte[] image, SQLiteDatabase db) {

        ContentValues cv = new ContentValues();
        cv.put(Contract.FeedEntry.LOCATION, name);
        cv.put(Contract.FeedEntry.KEY_IMAGE, image);
        db.insert(Contract.FeedEntry.TABLE_NAME, null, cv);
    }

    public ArrayList<String> getCoordinates(SQLiteDatabase db) {
        String query = "SELECT " + Contract.FeedEntry.LOCATION +
                " FROM " + Contract.FeedEntry.TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        ArrayList<String> locationList = new ArrayList<>();
        while (c.moveToNext()) {
            String coordinates = c.getString(c.getColumnIndex(Contract.FeedEntry.LOCATION));
            locationList.add(coordinates);
        }
        c.close();
        return locationList;
    }

    public ArrayList<Bitmap> getPictures(SQLiteDatabase db) {
        String query = "SELECT " + Contract.FeedEntry.KEY_IMAGE +
                " FROM " + Contract.FeedEntry.TABLE_NAME;
        Cursor c = db.rawQuery(query, null);
        ArrayList<Bitmap> imagesList = new ArrayList<>();

        while (c.moveToNext()) {

            byte[] img = c.getBlob(c.getColumnIndex(Contract.FeedEntry.KEY_IMAGE));
            Bitmap bmp = BitmapFactory.decodeByteArray(img, 0, img.length);
            imagesList.add(bmp);
        }
        c.close();
        return imagesList;
    }
    public boolean isRowExists(SQLiteDatabase db) {
        String query = "SELECT " + Contract.FeedEntry.KEY_IMAGE +
                " FROM " + Contract.FeedEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

}
