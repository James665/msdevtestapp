package com.example.jamesb.msdevtestapp.utils;


import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;


public class Utils {

    public Utils() {
    }


    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));

        return Radius * c;
    }

    public LatLng getLatLng(String coordinates) {
        double longitude, latitude;
        String asd[] = coordinates.split(",");
        longitude = Double.parseDouble(asd[0]);
        latitude = Double.parseDouble(asd[1]);
        LatLng point = new LatLng(longitude, latitude);
        return point;
    }

    public byte[] convertImageFormat(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

}
