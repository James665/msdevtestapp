package com.example.jamesb.msdevtestapp.fragments;


import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.jamesb.msdevtestapp.R;
import com.example.jamesb.msdevtestapp.TotalScreenActivity;
import com.example.jamesb.msdevtestapp.database.DatabaseHelper;
import com.example.jamesb.msdevtestapp.utils.Constants;


import com.example.jamesb.msdevtestapp.utils.DatabaseUtils;
import com.example.jamesb.msdevtestapp.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class MainScreenFragment extends Fragment implements LocationListener, View.OnClickListener {

    private ImageView mSelectedPic;
    private ImageView mSearch;
    private EditText mPictureLink;
    private ProgressBar mProgressBar;
    private LocationManager locationManager;
    private Location loc;
    private double longitude, latitude;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;
    private Utils utils;
    private DatabaseUtils dbUtils;

    public MainScreenFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);

        init(view);


        return view;
    }

    private void init(View view) {
        Button mShowFragment = (Button) view.findViewById(R.id.showMap);
        Button mTotalScreen = (Button) view.findViewById(R.id.totalscreen);
        Button mLocation = (Button) view.findViewById(R.id.location);
        mPictureLink = (EditText) view.findViewById(R.id.link);
        mSearch = (ImageView) view.findViewById(R.id.search_Bttn);
        mSelectedPic = (ImageView) view.findViewById(R.id.picture);
        ImageView mPopupMenuBttn = (ImageView) view.findViewById(R.id.popup_menu_btn);
        Button mAddToDataBase = (Button) view.findViewById(R.id.add_to_db);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);

        mPopupMenuBttn.setOnClickListener(this);
        mSearch.setOnClickListener(this);
        mShowFragment.setOnClickListener(this);
        mTotalScreen.setOnClickListener(this);
        mLocation.setOnClickListener(this);
        mAddToDataBase.setOnClickListener(this);

        databaseHelper = new DatabaseHelper(getActivity());
        db = databaseHelper.getWritableDatabase();
        dbUtils = new DatabaseUtils();
        utils = new Utils();

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (loc != null) {
            longitude = loc.getLongitude();
            latitude = loc.getLatitude();
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), getString(R.string.no_permission), Toast.LENGTH_SHORT);
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.search_Bttn:

                String url = mPictureLink.getText().toString();
                if (!url.isEmpty()) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity())
                            .load(url)
                            .into(mSelectedPic, new Callback() {

                                @Override
                                public void onSuccess() {
                                    mProgressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    Toast.makeText(getActivity(), getString(R.string.wrong_url), Toast.LENGTH_LONG);
                                }
                            });
                    mSearch.setVisibility(View.INVISIBLE);
                    mPictureLink.setText("");
                    mPictureLink.setVisibility(View.INVISIBLE);

                }
                break;

            case R.id.showMap:
                openDB();
                if (dbUtils.isRowExists(db)) {
                    db.close();
                    MapScreenFragment msf = new MapScreenFragment();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
                    transaction.replace(R.id.fragment_container, msf);
                    transaction.commit();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_rows), Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.totalscreen:
                Intent intent = new Intent(getActivity(), TotalScreenActivity.class);
                startActivity(intent);
                break;

            case R.id.location:
                String msg = getString(R.string.new_latitude) + latitude +
                        getString(R.string.new_longitude) + longitude;
                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                break;

            case R.id.add_to_db:
                if (longitude != 0 && latitude != 0) {

                    String coordinates = latitude + ", " + longitude;
                    if (mSelectedPic.getDrawable() != null) {
                        Bitmap bitmap = ((BitmapDrawable) mSelectedPic.getDrawable()).getBitmap();

                        byte[] pic = utils.convertImageFormat(bitmap);
                        openDB();
                        dbUtils.addDataToDatabase(coordinates, pic, db);
                        mSelectedPic.setImageResource(0);
                        mSearch.setVisibility(View.INVISIBLE);
                        mPictureLink.setText("");
                        mPictureLink.setVisibility(View.INVISIBLE);
                        db.close();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.select_image), Toast.LENGTH_SHORT)
                                .show();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.gps_may_not_work), Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case R.id.popup_menu_btn:
                showPopupMenu(v);
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getActivity(), getString(R.string.gps_on),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getActivity(), getString(R.string.gps_off),
                Toast.LENGTH_SHORT).show();
    }


    private void showPopupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menu1:
                        mSearch.setVisibility(View.INVISIBLE);
                        mPictureLink.setText("");
                        mPictureLink.setVisibility(View.INVISIBLE);
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        intent.putExtra("crop", "true");
                        intent.putExtra("aspectX", 0);
                        intent.putExtra("aspectY", 0);
                        intent.putExtra("outputX", 200);
                        intent.putExtra("outputY", 150);

                        try {

                            intent.putExtra("return-data", true);
                            startActivityForResult(intent, Constants.RESULT_LOAD_IMAGE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    case R.id.menu2:
                        mPictureLink.setVisibility(View.VISIBLE);
                        mSearch.setVisibility(View.VISIBLE);
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == Constants.RESULT_LOAD_IMAGE) {
                Bundle extras2 = data.getExtras();
                if (extras2 != null) {
                    Bitmap photo = extras2.getParcelable("data");
                    mSelectedPic.setImageBitmap(photo);
                }
            }
        }
    }

    private void openDB() {
        db = databaseHelper.getWritableDatabase();
    }

}
