package com.example.jamesb.msdevtestapp;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jamesb.msdevtestapp.adapters.RecyclerViewAdapter;
import com.example.jamesb.msdevtestapp.database.DatabaseHelper;
import com.example.jamesb.msdevtestapp.utils.DatabaseUtils;
import com.example.jamesb.msdevtestapp.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class TotalScreenActivity extends AppCompatActivity {
    private RecyclerView.LayoutManager mLayoutManager;
    private SQLiteDatabase db;
    private DatabaseUtils dbUtils;
    private Utils utils;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_screen);

        TextView mDayDistanceLAbel = (TextView) findViewById(R.id.displayDistance);
        databaseHelper = new DatabaseHelper(TotalScreenActivity.this);
        db = databaseHelper.getReadableDatabase();
        dbUtils = new DatabaseUtils();
        utils = new Utils();
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        ArrayList<Bitmap> bitmapArray = dbUtils.getPictures(db);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(bitmapArray);
        rv.setAdapter(adapter);


        ArrayList<String> locationList = dbUtils.getCoordinates(db);

        if (locationList.size() >= 1) {
            double dayDistance = getDayDistance(locationList);

            mDayDistanceLAbel.setText(String.valueOf(roundDistance(dayDistance)));
        } else {
            Toast.makeText(TotalScreenActivity.this, getString(R.string.only_one_point), Toast.LENGTH_SHORT)
                    .show();
            mDayDistanceLAbel.setText("0");
        }
        db.close();
    }

    private double getDayDistance(ArrayList<String> location) {
        double resultDistance = 0;

        for (int i = 0; i < location.size() - 1; i++) {
            LatLng startP = utils.getLatLng(location.get(i));
            LatLng endP = utils.getLatLng(location.get(i + 1));
            resultDistance += utils.CalculationByDistance(startP, endP);
        }
        return resultDistance;
    }

    private double roundDistance(double distance) {
        distance = distance * 1000;
        int i = (int) Math.round(distance);
        distance = (double) i / 1000;
        return distance;
    }
}
