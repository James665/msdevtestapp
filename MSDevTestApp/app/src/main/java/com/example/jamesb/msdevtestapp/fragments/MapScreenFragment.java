package com.example.jamesb.msdevtestapp.fragments;


import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import android.graphics.Matrix;
import android.os.Bundle;
import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jamesb.msdevtestapp.database.DatabaseHelper;
import com.example.jamesb.msdevtestapp.utils.DatabaseUtils;
import com.example.jamesb.msdevtestapp.utils.Utils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.example.jamesb.msdevtestapp.R;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.util.ArrayList;


public class MapScreenFragment extends Fragment implements OnMapReadyCallback {

    private SQLiteDatabase db;
    private DatabaseUtils dbUtils;
    private DatabaseHelper databaseHelper;
    private ArrayList<Bitmap> icons;
    private ArrayList<String> stringCoordinates;
    private Utils utils;

    public MapScreenFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(getActivity());
        db = databaseHelper.getReadableDatabase();
        dbUtils = new DatabaseUtils();
        utils = new Utils();
        icons = dbUtils.getPictures(db);
        stringCoordinates = dbUtils.getCoordinates(db);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        MapFragment mMapFragment = MapFragment.newInstance();
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.map, mMapFragment);
        transaction.commitAllowingStateLoss();
        mMapFragment.getMapAsync(this);

        return v;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        ArrayList<LatLng> locationList = new ArrayList<>();
        for (String s : stringCoordinates) {
            locationList.add(utils.getLatLng(s));
        }

        int N = icons.size();
        ArrayList<Marker> markers = new ArrayList<>();
        if (N == locationList.size()) {

            for (int i = 0; i < N; i++) {
                Marker marker = googleMap.addMarker(new MarkerOptions().position(locationList.get(i))
                        .title(getString(R.string.marker + i))
                        .snippet(getString(R.string.snippet + i))
                        .icon(BitmapDescriptorFactory.fromBitmap(getResizedBitmap(icons.get(i), 100, 100))));
                markers.add(marker);

            }

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 0;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cu);
            db.close();
        }

    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

}


